#!/bin/sh
source "/var/volatile/project_eris.cfg"

WORKDIR="/var/volatile/launchtmp"
BRUTALBUILD="brutalv20b_mmc.pk3"
IDKFA="MODS/IDKFAv2.wad"

cd "${WORKDIR}"

echo "launch_StockUI" > "/tmp/launchfilecommand"

[ ! -f "${WORKDIR}/.config/zdoom.ini" ] && cp "${WORKDIR}/.config/zdoom_def.ini" "${WORKDIR}/.config/zdoom.ini"

# Define mods
MODS=" "
for f in "${WORKDIR}/MODS/"*".wad"; do
  [ -e "${f}" ] || continue
  MODS="${MODS} ${f}"
  MODS_LOADED=TRUE
done

# Load the first base wad in the WAD directory.
WAD="$(ls ${WORKDIR}/WAD/ | grep -i .wad | head -1)"
[ -z ${WAD} ] && exit 1
WAD="${WORKDIR}/WAD/${WAD}"

chmod +x "zdoom"
echo -n 2 > "/data/power/disable"
HOME="${WORKDIR}" LD_LIBRARY_PATH="${WORKDIR}/lib" ./zdoom -iwad ${WAD} -file ${BRUTALBUILD} ${IDKFA} ${MODS} +vid_fps 1 &> "${RUNTIME_LOG_PATH}/zdoom.log"
echo -n 1 > "/data/power/disable"